// Industry grade - Chat application
// Refer: https://github.com/dkhd/node-group-chat

var cookieParser = require('cookie-parser');
var cors = require('cors');
var createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const logkat = require('@logkat/logger');
var path = require('path');
const setup = require('./setup');
setup.init();  // Setting up the environment

// Importing all the routes created
var indexRouter = require('./routes/index');

var app = express();

// Setting up CORS - Cross Origin Resource Sharing
app.use(cors());
app.options('*', cors());

// Setting up the views to look for the templates
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Setting up logger.
app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Registering Routes
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
  });

// Handling errors
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
module.exports = app;