const { registerConnectionSocket } = require('./io');

function init(server) {
  var io = require('socket.io')(server);

  io.sockets.on('connection', function(socket) {
    registerConnectionSocket(io, socket);
  });
}

module.exports = {
  init: init
}