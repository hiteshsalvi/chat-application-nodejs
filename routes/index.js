var express = require('express');
const logkat = require('@logkat/logger');
const path = require('path');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', { host: req.headers.host});
});

router.get('/favicon.ico', (req, res) => {
    res.sendFile(path.resolve('./public/images/favicon.ico'));
});

module.exports = router;