const logkat = require('@logkat/logger');

function registerConnectionSocket(io, socket) {
  socket.on('username', function(username) {
    socket.username = username;
    io.emit('is_online', '🔵 <i>' + socket.username + ' joined the chat..</i>');
  });

  socket.on('disconnect', function(username) {
    io.emit('is_online', '🔴 <i>' + socket.username + ' left the chat..</i>');
  });

  socket.on('chat_message', function(message) {
    io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message);
  });

  socket.on('typing', function(username) {
    io.emit('is_typing', username);
  });
}

module.exports = {
  registerConnectionSocket: registerConnectionSocket
}